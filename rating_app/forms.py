from rating_app.models import Reviews
from django.forms import ModelForm


class ReviewForm(ModelForm):
    class Meta:
        model = Reviews
        fields = [
            'title',
            'game_cover',
            'review',
            'rating',
        ]

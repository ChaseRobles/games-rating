from django.contrib import admin
from rating_app.models import Reviews
# Register your models here.

@admin.register(Reviews)
class Reviews(admin.ModelAdmin):
    list_display = [
        'title',
        'id',
    ]

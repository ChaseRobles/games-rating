from django.db import models
from django.conf import settings


class Reviews(models.Model):
    title = models.CharField(max_length=200)
    game_cover = models.URLField()
    review = models.TextField()
    review_published_date = models.DateTimeField(auto_now_add=True)
    reviewer = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='reviews',
        on_delete=models.CASCADE,
        null=True,
    )
    rating = models.FloatField()
# Create your models here.

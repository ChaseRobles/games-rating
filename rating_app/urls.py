from django.urls import path
from .views import list, details, edit_review, create_review

urlpatterns = [
    path('game_list/', list, name = 'game_list'),
    path('game_list/details/<int:id>', details, name = 'details'),
    path('game_list/details/<int:id>/edit/', edit_review, name = 'edits'),
    path('game_list/create', create_review, name = 'create')
]

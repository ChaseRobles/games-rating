from django.shortcuts import render, redirect
from rating_app.models import Reviews
from rating_app.forms import ReviewForm




def list(request):
    reviews = Reviews.objects.all()
    context = {
        'list': reviews,
    }
    return render(request, 'rating_app/game_list.html', context)
# Create your views here.
def details(request, id):
    reviews = Reviews.objects.get(id=id)
    context = {'reviews_objects': reviews}
    return render(request, 'rating_app/details.html', context)


def edit_review(request, id):
    reviews = Reviews.objects.get(id=id)
    if request.method == 'POST':
        form = ReviewForm(request.POST , instance = reviews)
        if form.is_valid():
            form.save()
            return redirect('game_list')
    else:
        form = ReviewForm(instance = reviews)

    context={'form': form, 'reviews': reviews,}
    return render(request,'rating_app/edit.html', context)

def create_review(request):
    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            reviews = form.save(False)
            reviews.reviewer = request.user
            form.save()
            return redirect('game_list')
    else:
        form = ReviewForm()
    context = {
        'form': form,
    }
    return render(request, 'rating_app/create.html', context)
